import React, {Component} from 'react'
import NewSingle from './NewSingle'
import Grid from 'material-ui/Grid';
import Error from './Error'

class News extends Component {

  //props that could be passed in  news = props.news
  constructor(props){
    super(props)

    this.state = {
      news: [],
      error: false
    }
  }

  //run after the component is rendered.
  componentDidMount(){
    const url = `https://newsapi.org/v2/${this.props.news.type}?${this.props.news.query}&apiKey=39c54dcbe001449096e4647a361bc81a`

    fetch(url)
      .then((response) => {
       return response.json()
      })
      .then((data) => {
        this.setState({
          news: data.articles
        })
      })
      .catch((error) => {
        this.setState({
          error: false
        })
      })
  }

  renderItems() {

    if (!this.state.error) {
      return this.state.news.map((item) => (
        <NewSingle key={item.url} item={item}/>
      ))
    } else {
      return <Error />
    }

  }

  render() {
    return (
      <div className="news-feed">
        <Grid container>
        {this.renderItems()}
        </Grid>
      </div>
    )
  }

}

export default News
