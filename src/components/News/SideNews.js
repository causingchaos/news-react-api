import React, {Component} from 'react'
import axios from 'axios'
import SingleSide from './SingleSide'
import Grid from 'material-ui/Grid';
import Error from './Error'


class SideNews extends Component {

  //props that could be passed in  news = props.news
  constructor(props){
    super(props)

    this.state = {
      sidenews: [],
      error: false,
    }
  }

  //run after the component is rendered.
  componentDidMount(){
    const url = `https://newsapi.org/v2/${this.props.news.type}?${this.props.news.query}&apiKey=39c54dcbe001449096e4647a361bc81a`

    //can use post, and don't need to convert the json.
    /*
    axios.post(url, {
      data: {
        news: {
          title: "sibisbios",
          description: '8383838383'
        }
      }
    })
    */

    //Use axios to just grab json data, don't need to convert, it does it for us
    axios.get(url)
      .then((response) => {
        this.setState({
          sidenews: response.data.articles
        })
      })
      .catch((error) => {
        this.setState({
          error: false
        })
      })

  }

  renderItems() {
    if (this.state.error === true){
      return <Error/>
    }else {

      return this.state.sidenews.map((item) => (
        <SingleSide key={item.url} item={item}/>
      ))
    }
  }

  render() {

      return (
        <div className="news-feed">
          <Grid container>
            {this.renderItems()}
          </Grid>
        </div>
      )
    }

}

export default SideNews
