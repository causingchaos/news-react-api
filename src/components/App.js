import React, { Component } from 'react';
import './App.css';
import News from './News/News'
import SideNews from './News/SideNews'

//Material design imports
import TopAppBar from './Navigation/TopNavigation'
import CssBaseline from 'material-ui/CssBaseline'
import { withStyles } from 'material-ui/styles'
import PropTypes from 'prop-types'
import Grid from 'material-ui/Grid'

const styles = {
  App: {
    backgroundColor: "white",
  }
}

class App extends Component {
  constructor(props){
    super(props)

    // i.e. /everything?
    this.state={
      news1:{
        type: 'top-headlines',
        query: 'sources=bbc-news'
      },
      news2: {
        type: 'everything',
        query: 'domains=techcrunch.com&language=en'
      },
      news3: {
        type: 'everything',
        query: 'domains=comicbookmovie.com&language=en'
      }
    }
  }

  render() {

    return (
      <div className={this.props.classes.App}>
        <CssBaseline/>
        <TopAppBar />
        <Grid container>
          <Grid item xs={9}>
            <News news={this.state.news1} />
            <News news={this.state.news2} />
          </Grid>
          <Grid item xs={3}>
            <SideNews news={this.state.news3}/>
          </Grid>
        </Grid>
      </div>
    )
  }
}

App.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(App);
