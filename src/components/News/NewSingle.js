import React from 'react'
import Grid from 'material-ui/Grid'
import Card, {CardContent, CardMedia, CardActions} from 'material-ui/Card'
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button'

const NewSingle = ({item}) => (
  <Grid item xs={12} sm={6} md={4} lg={3}>
    <Card className="card"  style={{maxWidth: 345}}>
      <CardMedia className="card-image"
                 style={{height: 200}}
                 image={item.urlToImage}
                 title={item.title}
      >
      </CardMedia>
      <CardContent className="card-content">
        <Typography variant="headline" component="h6">
          {item.title}
        </Typography>
        <Typography component="p">
          {item.description}
        </Typography>
      </CardContent>
      <CardActions className="card-actions" >
        <Button size="small" color="primary" href={item.url} target="_blank"
        >
          Read More
        </Button>
      </CardActions>
    </Card>
  </Grid>
)

export default NewSingle
